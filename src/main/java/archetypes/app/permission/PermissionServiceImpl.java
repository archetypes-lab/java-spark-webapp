package archetypes.app.permission;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

public class PermissionServiceImpl implements PermissionService {

    private final PermissionRepository repository;

    public PermissionServiceImpl(PermissionRepository repository) {
        this.repository = repository;
    }

    @Transactional(readOnly = true)
    @Override
    public List<Permission> findByProfileId(Integer profileId) {
        return this.repository.findByProfileId(profileId);
    }

}
