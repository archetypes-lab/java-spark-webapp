package archetypes.app.permission;

import lombok.Data;

@Data
public class Permission {

    private Integer id;

    private String code;

    private String description;
    
}
