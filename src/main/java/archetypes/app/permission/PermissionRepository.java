package archetypes.app.permission;

import java.util.List;

public interface PermissionRepository {

    List<Permission> findByProfileId(Integer profileId);

}
