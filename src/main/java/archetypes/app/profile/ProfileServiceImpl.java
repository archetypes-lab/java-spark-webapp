package archetypes.app.profile;

import org.springframework.transaction.annotation.Transactional;

public class ProfileServiceImpl implements ProfileService {

    private final ProfileRepository repository;

    public ProfileServiceImpl(ProfileRepository repository) {
        this.repository = repository;
    }

    @Transactional(readOnly = true)
    @Override
    public Profile findById(Integer id) {
        return this.repository.findById(id);
    }

}
