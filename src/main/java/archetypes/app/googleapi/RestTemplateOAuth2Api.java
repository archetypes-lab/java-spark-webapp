package archetypes.app.googleapi;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

public class RestTemplateOAuth2Api implements OAuth2Api {

    private static final String BASE_URL = "https://oauth2.googleapis.com";

    private static final String TOKEN_URL = BASE_URL + "/token";

    private final RestTemplate restTemplate;

    public RestTemplateOAuth2Api(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public OAuth2TokenResponse token(OAuth2TokenRequest request) {
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        var map = new LinkedMultiValueMap<String, String>();
        map.add("code", request.getCode());
        map.add("client_id", request.getClientId());
        map.add("client_secret", request.getClientSecret());
        map.add("redirect_uri", request.getRedirectUri());
        map.add("grant_type", request.getGrantType());

        var entity = new HttpEntity<MultiValueMap<String, String>>(map, headers);

        return restTemplate.postForObject(TOKEN_URL, entity, OAuth2TokenResponse.class);
    }

}
