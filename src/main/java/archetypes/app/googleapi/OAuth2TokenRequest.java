package archetypes.app.googleapi;

import lombok.Data;

@Data
public class OAuth2TokenRequest {

    private String code;

    private String clientId;

    private String clientSecret;

    private String redirectUri;

    private String grantType;
    
}
