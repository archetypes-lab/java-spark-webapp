package archetypes.app.demo;

public class DemoServiceImpl implements DemoService {

    @Override
    public String greetings(String name) {
        return String.format("hello %s from service", name);
    }

}
