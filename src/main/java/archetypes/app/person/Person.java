package archetypes.app.person;

import java.time.LocalDate;

import lombok.Data;

@Data
public class Person {

    private Long id;

    private Integer profileId;

    private Integer rut;

    private String email;

    private String firstName;

    private String lastName;

    private LocalDate birthday;

    private Boolean active;

}
