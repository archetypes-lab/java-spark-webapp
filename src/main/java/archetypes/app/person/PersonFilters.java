package archetypes.app.person;

import java.time.LocalDate;

import lombok.Data;

@Data
public class PersonFilters {
    
    private Long id;

    private String name;

    private Integer rut;

    private String email;

    private Boolean isActivate;

    private LocalDate beginBirthday;

    private LocalDate endBirthday;

}
