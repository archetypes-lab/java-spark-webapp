package archetypes.app.person;

import java.util.List;
import java.util.Optional;

public interface PersonService {
    List<Person> getAll();

    List<Person> findByName(String name);

    List<Person> findByFilters(PersonFilters filters);

    Optional<Person> findByEmail(String email);

    Person save(Person person);

    void delete(long id);

}
