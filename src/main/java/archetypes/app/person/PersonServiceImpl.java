package archetypes.app.person;

import java.util.List;
import java.util.Optional;

import org.springframework.transaction.annotation.Transactional;

public class PersonServiceImpl implements PersonService {

    private final PersonRepository repository;

    public PersonServiceImpl(PersonRepository repository) {
        this.repository = repository;
    }

    @Transactional(readOnly = true)
    @Override
    public List<Person> getAll() {
        return this.repository.getAll();
    }

    @Transactional(readOnly = true)
    @Override
    public List<Person> findByName(String name) {
        return this.repository.findByName(name);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Person> findByFilters(PersonFilters filters) {
        return this.repository.findByFilters(filters);
    }

    @Transactional(readOnly = true)
    @Override
    public Optional<Person> findByEmail(String email) {
        return this.repository.findByEmail(email);
    }

    @Transactional(readOnly = false)
    @Override
    public Person save(Person person) {
        if (person.getId() == null) {
            return this.repository.insert(person);
        } else {
            return this.repository.update(person);
        }
    }

    @Transactional(readOnly = false)
    @Override
    public void delete(long id) {
        this.repository.delete(id);
    }

}
