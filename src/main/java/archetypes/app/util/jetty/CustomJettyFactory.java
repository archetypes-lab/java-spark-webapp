package archetypes.app.util.jetty;

import org.eclipse.jetty.util.thread.ThreadPool;

import spark.ExceptionMapper;
import spark.embeddedserver.EmbeddedServer;
import spark.embeddedserver.EmbeddedServerFactory;
import spark.embeddedserver.jetty.EmbeddedJettyServer;
import spark.embeddedserver.jetty.JettyHandler;
import spark.embeddedserver.jetty.JettyServerFactory;
import spark.http.matching.MatcherFilter;
import spark.route.Routes;
import spark.staticfiles.StaticFilesConfiguration;

public class CustomJettyFactory implements EmbeddedServerFactory {

    private final JettyServerFactory serverFactory;
    private ThreadPool threadPool;
    private boolean httpOnly = true;

    public CustomJettyFactory() {
        this.serverFactory = new JettyServer();
    }

    public CustomJettyFactory(JettyServerFactory serverFactory) {
        this.serverFactory = serverFactory;
    }

    public EmbeddedServer create(Routes routeMatcher,
            StaticFilesConfiguration staticFilesConfiguration,
            ExceptionMapper exceptionMapper,
            boolean hasMultipleHandler) {
        MatcherFilter matcherFilter = new MatcherFilter(routeMatcher, staticFilesConfiguration, exceptionMapper, false,
                hasMultipleHandler);
        matcherFilter.init(null);

        JettyHandler handler = new JettyHandler(matcherFilter);
        handler.getSessionCookieConfig().setHttpOnly(httpOnly);
        return new EmbeddedJettyServer(serverFactory, handler).withThreadPool(threadPool);
    }

    /**
     * Sets optional thread pool for jetty server. This is useful for overriding the
     * default thread pool
     * behaviour for example
     * io.dropwizard.metrics.jetty9.InstrumentedQueuedThreadPool.
     *
     * @param threadPool thread pool
     * @return Builder pattern - returns this instance
     */
    public CustomJettyFactory withThreadPool(ThreadPool threadPool) {
        this.threadPool = threadPool;
        return this;
    }

    public CustomJettyFactory withHttpOnly(boolean httpOnly) {
        this.httpOnly = httpOnly;
        return this;
    }

}
