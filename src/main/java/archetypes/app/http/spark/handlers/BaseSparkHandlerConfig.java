package archetypes.app.http.spark.handlers;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Getter;
import lombok.Setter;

public class BaseSparkHandlerConfig {
    
    @Getter
    @Setter
    private spark.Service spark;

    @Getter
    @Setter
    private spark.TemplateEngine templateEngine;

    @Getter
    @Setter
    private ObjectMapper mapper;

}
