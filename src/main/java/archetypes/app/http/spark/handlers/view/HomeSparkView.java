package archetypes.app.http.spark.handlers.view;

import java.util.Collections;

import archetypes.app.http.spark.handlers.BaseSparkHandler;
import archetypes.app.http.spark.handlers.BaseSparkHandlerConfig;
import spark.Request;
import spark.Response;

public class HomeSparkView extends BaseSparkHandler {

    public HomeSparkView(BaseSparkHandlerConfig config) {
        super(config);
        spark().get("/", this::homeView);
        spark().get("/home", this::homeView);
    }

    public String homeView(Request request, Response response) {
        return renderHtml(response, Collections.emptyMap(), "views/home.ftl");
    }
    
}
