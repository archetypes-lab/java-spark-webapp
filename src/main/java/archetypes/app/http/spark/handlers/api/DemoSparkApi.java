package archetypes.app.http.spark.handlers.api;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import archetypes.app.demo.DemoService;
import archetypes.app.http.spark.handlers.BaseSparkHandler;
import archetypes.app.http.spark.handlers.BaseSparkHandlerConfig;
import spark.Request;
import spark.Response;

public class DemoSparkApi extends BaseSparkHandler {

    private static final Logger log = LoggerFactory.getLogger(DemoSparkApi.class);

    private final DemoService demoService;

    public DemoSparkApi(BaseSparkHandlerConfig config, DemoService demoService) {
        super(config);
        this.demoService = demoService;

        spark().path("/api/v1/hello", () -> {
            spark().get("/json/:name", this::greetingJson);
            spark().get("/:name", this::greeting);
        });

    }

    public String greeting(Request request, Response response) {
        log.debug("-> enter greeting");
        var name = request.params(":name");
        var greetings = this.demoService.greetings(name);
        return renderText(response, greetings);
    }

    public String greetingJson(Request request, Response response) {
        log.debug("-> enter greetingJson");
        var name = request.params(":name");
        var greetings = this.demoService.greetings(name);
        return renderJson(response, Map.of("greetings", greetings));
    }

}
