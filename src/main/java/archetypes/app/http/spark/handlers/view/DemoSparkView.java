package archetypes.app.http.spark.handlers.view;

import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import archetypes.app.http.spark.handlers.BaseSparkHandler;
import archetypes.app.http.spark.handlers.BaseSparkHandlerConfig;
import spark.Request;
import spark.Response;

public class DemoSparkView extends BaseSparkHandler {

    private static final Logger log = LoggerFactory.getLogger(DemoSparkView.class);

    public DemoSparkView(BaseSparkHandlerConfig config) {
        super(config);

        spark().get("/demo", this::demoView);
    }

    public String demoView(Request request, Response response) {
        log.debug("-> enter demoView");
        return renderHtml(response, Collections.emptyMap(), "views/demo.ftl");
    }
    
}
