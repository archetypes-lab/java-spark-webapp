package archetypes.app.http.spark.handlers.view;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import archetypes.app.http.spark.handlers.BaseSparkHandler;
import archetypes.app.http.spark.handlers.BaseSparkHandlerConfig;
import spark.Request;
import spark.Response;

public class OpenIdCallbackSparkView extends BaseSparkHandler {

    private static final Logger log = LoggerFactory.getLogger(OpenIdCallbackSparkView.class);

    public OpenIdCallbackSparkView(BaseSparkHandlerConfig config) {
        super(config);
        spark().get("/openid-callback", this::openIdCallbackView);
    }

    public String openIdCallbackView(Request request, Response response) {
        var model = new HashMap<String, Object>();
        model.put("code", request.queryParams("code"));
        model.put("state", request.queryParams("state"));
        log.debug("model: {}", model);
        return renderHtml(response, model, "views/openidcallback.ftl");
    }

}
