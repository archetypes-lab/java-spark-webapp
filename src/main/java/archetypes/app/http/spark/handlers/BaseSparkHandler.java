package archetypes.app.http.spark.handlers;

import java.io.IOException;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import spark.ModelAndView;
import spark.Response;

public abstract class BaseSparkHandler {

    private final BaseSparkHandlerConfig config;

    protected BaseSparkHandler(BaseSparkHandlerConfig config) {
        this.config = config;
    }

    protected String renderHtml(Response response, Map<String, Object> model, String templatePath) {
        response.type("text/html");
        var modelAndView = new ModelAndView(model, templatePath);
        return this.config.getTemplateEngine().render(modelAndView);
    }

    protected String renderText(Response response, String text) {
        response.type("text/plain");
        return text;
    }

    protected String renderJson(Response response, Object model) {
        response.type("application/json");
        var mapper = new ObjectMapper()
                .registerModule(new JavaTimeModule());
        try {
            return mapper.writeValueAsString(model);
        } catch (JsonProcessingException ex) {
            throw new IllegalStateException(ex);
        }
    }

    protected String emptyResponse(Response response) {
        return renderText(response, "ok");
    }

    protected spark.Service spark() {
        return this.config.getSpark();
    }

    protected <T> T mapJsonBodyToObject(spark.Request request, Class<T> clazz) {
        var bytes = request.bodyAsBytes();
        try {
            return this.config.getMapper().readValue(bytes, clazz);
        } catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }

}
