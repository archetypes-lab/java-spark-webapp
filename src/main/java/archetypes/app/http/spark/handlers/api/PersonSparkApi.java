package archetypes.app.http.spark.handlers.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import archetypes.app.http.spark.handlers.BaseSparkHandler;
import archetypes.app.http.spark.handlers.BaseSparkHandlerConfig;
import archetypes.app.person.Person;
import archetypes.app.person.PersonFilters;
import archetypes.app.person.PersonService;
import spark.Request;
import spark.Response;

public class PersonSparkApi extends BaseSparkHandler {

    private static final Logger log = LoggerFactory.getLogger(PersonSparkApi.class);

    private final PersonService personService;

    public PersonSparkApi(BaseSparkHandlerConfig config, PersonService personService) {
        super(config);
        this.personService = personService;

        spark().path("/api/v1/person", ()-> {
            spark().post("", this::savePerson);
            spark().get("/all", this::getAllPersons);
            spark().get("/:id", this::getPersonById);
            spark().delete("/:id", this::deletePerson);
        });
        
    }

    public String savePerson(Request request, Response response) {
        log.debug("-> enter savePerson");
        var inputPerson = mapJsonBodyToObject(request, Person.class);
        personService.save(inputPerson);
        return emptyResponse(response);
    }

    public String getAllPersons(Request request, Response response) {
        log.debug("-> enter getAllPerson");
        var persons = personService.getAll();
        return renderJson(response, persons);
    }

    public String getPersonById(Request request, Response response) {
        log.debug("-> enter getPerson");
        var idParam = request.params(":id");
        var id = Long.parseLong(idParam);
        var filters = new PersonFilters();
        filters.setId(id);
        var persons = personService.findByFilters(filters);
        var person = persons.isEmpty() ? null : persons.get(0);
        return renderJson(response, person);
    }

    public String deletePerson(Request request, Response response) {
        log.debug("-> enter deletePerson");
        var idParam = request.params(":id");
        var id = Long.parseLong(idParam);
        personService.delete(id);
        return emptyResponse(response);
    }

}
