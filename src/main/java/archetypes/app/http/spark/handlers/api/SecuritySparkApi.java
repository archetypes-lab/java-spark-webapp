package archetypes.app.http.spark.handlers.api;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import archetypes.app.http.spark.handlers.BaseSparkHandler;
import archetypes.app.http.spark.handlers.BaseSparkHandlerConfig;
import archetypes.app.http.spark.utils.security.OpenIdCallbackRequest;
import archetypes.app.http.spark.utils.security.SparkSecurityUtils;
import spark.Request;
import spark.Response;

public class SecuritySparkApi extends BaseSparkHandler {

    private static final Logger log = LoggerFactory.getLogger(SecuritySparkApi.class);

    private final SparkSecurityUtils securityUtils;

    public SecuritySparkApi(BaseSparkHandlerConfig config, SparkSecurityUtils securityUtils) {
        super(config);
        this.securityUtils = securityUtils;

        spark().path("/api/v1/security", () -> {
            spark().get("/openid-url", this::getOpenIdLoginUrl);
            spark().post("/openid-callback", this::receiveOpenIdCallback);
            spark().get("/identity", this::getIdentity);
            spark().get("/logout", this::logout);
        });

    }

    public String getOpenIdLoginUrl(Request request, Response response) {
        log.debug("-> enter getOpenIdLoginUrl");
        var url = securityUtils.getLoginUrl(request);
        return renderJson(response, Map.of("url", url));
    }

    public String receiveOpenIdCallback(Request request, Response response) {
        log.debug("-> enter receiveOpenIdCallback");
        var openIdParams = mapJsonBodyToObject(request, OpenIdCallbackRequest.class);
        log.debug("openIdParams: {}", openIdParams);
        var identity = securityUtils.loginWithOpenIdCode(request, openIdParams.getCode(), openIdParams.getState());
        return renderJson(response, identity);
    }

    public String getIdentity(Request request, Response response) {
        log.debug("-> getIdentity");
        return renderJson(response, securityUtils.getIdentity(request));
    }

    public String logout(Request request, Response response) {
        log.debug("-> logout");
        securityUtils.logout(request);
        return emptyResponse(response);
    }

}
