package archetypes.app.http.spark.utils.security;

import spark.Request;

public interface SparkSecurityUtils {

    Identity getIdentity(Request request);
    
    String getLoginUrl(Request request);

    String getSessionState(Request request);

    Identity loginWithOpenIdCode(Request request, String code, String state);

    void logout(Request request);

}
