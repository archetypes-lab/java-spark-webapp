package archetypes.app.http.spark.utils.security;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.function.Predicate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import archetypes.app.openid.OpenIdService;
import archetypes.app.openid.User;
import archetypes.app.permission.PermissionService;
import archetypes.app.person.Person;
import archetypes.app.person.PersonService;
import archetypes.app.profile.Profile;
import archetypes.app.profile.ProfileService;
import spark.Request;

public class SparkSecurityUtilsImpl implements SparkSecurityUtils {

    private static final Logger log = LoggerFactory.getLogger(SparkSecurityUtilsImpl.class);

    private static final String SESSION_STATE_PARAM = "session-state";
    private static final String SESSION_IDENTITY_PARAM = "session-identity";

    private final OpenIdService openIdService;
    private final PersonService personService;
    private final ProfileService profileService;
    private final PermissionService permissionService;

    public SparkSecurityUtilsImpl(
            OpenIdService openIdService,
            PersonService personService,
            ProfileService profileService,
            PermissionService permissionService) {
        this.openIdService = openIdService;
        this.personService = personService;
        this.profileService = profileService;
        this.permissionService = permissionService;
    }

    @Override
    public Identity getIdentity(Request request) {
        var raw = request.session().attribute(SESSION_IDENTITY_PARAM);
        if (raw instanceof Identity) {
            return (Identity) raw;
        } else {
            return null;
        }
    }

    @Override
    public String getSessionState(Request request) {
        var session = request.session();
        var state = session.<String>attribute(SESSION_STATE_PARAM);
        if (state == null) {
            state = new BigInteger(130, new SecureRandom()).toString(32);
            session.attribute(SESSION_STATE_PARAM, state);
        }
        return state;
    }

    @Override
    public String getLoginUrl(Request request) {
        var state = getSessionState(request);
        return openIdService.getLoginUrl(state);
    }

    @Override
    public Identity loginWithOpenIdCode(Request request, String code, String state) {

        var actualSessionState = getSessionState(request);

        Predicate<String> stateValidator = stateParam -> stateParam != null && stateParam.equals(actualSessionState);

        var openIdUser = this.openIdService.processCallback(code, state, stateValidator);

        log.debug("openIdUser: {}", openIdUser);

        var person = getPersonOrCreateNew(openIdUser);

        var profile = profileService.findById(person.getProfileId());

        var permissions = permissionService.findByProfileId(profile.getId());

        var identity = new Identity();
        identity.setPerson(person);
        identity.setProfile(profile);
        identity.setPermissions(permissions);
        identity.setPictureUrl(openIdUser.getPictureUrl());

        request.session().attribute(SESSION_IDENTITY_PARAM, identity);

        return identity;
    }

    // private

    private Person getPersonOrCreateNew(User openIdUser) {
        var opPerson = this.personService.findByEmail(openIdUser.getEmail());
        if (opPerson.isPresent()) {
            return opPerson.get();
        }

        // create the person if not exists
        var person = new Person();
        person.setEmail(openIdUser.getEmail());
        person.setFirstName(openIdUser.getFirstName());
        person.setLastName(openIdUser.getLastName());
        person.setProfileId(Profile.ID_USER);
        person.setActive(true);

        return this.personService.save(person);
    }

    @Override
    public void logout(Request request) {
        request.session().invalidate();
    }

}
