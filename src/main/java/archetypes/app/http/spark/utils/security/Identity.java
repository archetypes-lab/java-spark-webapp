package archetypes.app.http.spark.utils.security;

import java.util.List;

import archetypes.app.permission.Permission;
import archetypes.app.person.Person;
import archetypes.app.profile.Profile;
import lombok.Data;

@Data
public class Identity {

    private Person person;

    private String pictureUrl;

    private Profile profile;

    private List<Permission> permissions;

}
