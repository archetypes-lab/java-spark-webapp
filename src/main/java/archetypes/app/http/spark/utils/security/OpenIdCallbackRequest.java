package archetypes.app.http.spark.utils.security;

import lombok.Data;

@Data
public class OpenIdCallbackRequest {
    
    private String code;

    private String state;

}
