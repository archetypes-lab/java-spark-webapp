package archetypes.app.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import archetypes.app.permission.PermissionRepository;
import archetypes.app.persistence.postgres.PermissionRepoPostgres;
import archetypes.app.persistence.postgres.PersonRepoPostgres;
import archetypes.app.persistence.postgres.ProfileRepoPostgres;
import archetypes.app.person.PersonRepository;
import archetypes.app.profile.ProfileRepository;

@Configuration
public class Repositories {

    @Bean
    public PersonRepository personRepoPostgres(NamedParameterJdbcTemplate jdbcTemplate) {
        return new PersonRepoPostgres(jdbcTemplate);
    }

    @Bean
    public ProfileRepository profileRepoPostgres(NamedParameterJdbcTemplate jdbcTemplate) {
        return new ProfileRepoPostgres(jdbcTemplate);
    }

    @Bean
    public PermissionRepository permissionRepoPostgres(NamedParameterJdbcTemplate jdbcTemplate) {
        return new PermissionRepoPostgres(jdbcTemplate);
    }

}
