package archetypes.app.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import archetypes.app.demo.DemoService;
import archetypes.app.demo.DemoServiceImpl;
import archetypes.app.permission.PermissionRepository;
import archetypes.app.permission.PermissionService;
import archetypes.app.permission.PermissionServiceImpl;
import archetypes.app.person.PersonRepository;
import archetypes.app.person.PersonService;
import archetypes.app.person.PersonServiceImpl;
import archetypes.app.profile.ProfileRepository;
import archetypes.app.profile.ProfileService;
import archetypes.app.profile.ProfileServiceImpl;

@Configuration
public class Services {

  @Bean
  public DemoService demoServiceImpl() {
    return new DemoServiceImpl();
  }

  @Bean
  public PersonService personServiceImppl(PersonRepository personRepository) {
    return new PersonServiceImpl(personRepository);
  }

  @Bean
  public ProfileService profileServiceImpl(ProfileRepository profileRepository) {
    return new ProfileServiceImpl(profileRepository);
  }

  @Bean
  public PermissionService permissionServiceImpl(PermissionRepository permissionRepository) {
    return new PermissionServiceImpl(permissionRepository);
  }

}
