package archetypes.app.config;

import javax.sql.DataSource;

import org.flywaydb.core.Flyway;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import archetypes.app.googleapi.RestTemplateOAuth2Api;
import archetypes.app.http.spark.handlers.BaseSparkHandlerConfig;
import archetypes.app.http.spark.utils.security.SparkSecurityUtils;
import archetypes.app.http.spark.utils.security.SparkSecurityUtilsImpl;
import archetypes.app.openid.GoogleOpenIdService;
import archetypes.app.openid.OpenIdConfig;
import archetypes.app.openid.OpenIdService;
import archetypes.app.permission.PermissionService;
import archetypes.app.person.PersonService;
import archetypes.app.profile.ProfileService;
import archetypes.app.util.jetty.CustomJettyFactory;
import archetypes.app.util.jwt.JavaJwtUtils;
import archetypes.app.util.jwt.JwtUtils;
import io.github.cdimascio.dotenv.Dotenv;
import spark.Service;
import spark.TemplateEngine;
import spark.embeddedserver.EmbeddedServerFactory;
import spark.embeddedserver.EmbeddedServers;
import spark.template.freemarker.FreeMarkerEngine;

@Configuration
public class Base {

    private static final Logger log = LoggerFactory.getLogger(Base.class);

    // base

    @Bean
    public Dotenv dotenv() {
        return Dotenv.configure().ignoreIfMissing().ignoreIfMalformed().load();
    }

    @Bean
    public ObjectMapper sparkObjectMapper() {
        return new ObjectMapper()
                .registerModule(new JavaTimeModule());
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public JwtUtils javaJwtUtils() {
        return new JavaJwtUtils();
    }

    // database

    @Bean
    public DataSource dataSource(Dotenv dotenv) {
        var config = new HikariConfig();
        config.setDriverClassName(dotenv.get("DATABASE_JDBC_DRIVER"));
        config.setJdbcUrl(dotenv.get("DATABASE_JDBC_URL"));
        config.setUsername(dotenv.get("DATABASE_JDBC_USERNAME"));
        config.setPassword(dotenv.get("DATABASE_JDBC_PASSWORD"));
        return new HikariDataSource(config);
    }

    @Bean
    public Object flywayMigration(DataSource dataSource) {
        var flyway = Flyway.configure().dataSource(dataSource).load();
        flyway.migrate();
        return flyway;
    }

    @Bean
    public NamedParameterJdbcTemplate jdbcTemplate(DataSource dataSource) {
        return new NamedParameterJdbcTemplate(dataSource);
    }

    // http

    @Bean
    public EmbeddedServerFactory customJettyFactory() {
        return new CustomJettyFactory();
    }

    @Bean
    public spark.Service sparkHttp(EmbeddedServerFactory customJettyFactory) {

        EmbeddedServers.add(
                EmbeddedServers.Identifiers.JETTY,
                customJettyFactory);

        log.info("starting spark!");
        var spark = Service.ignite();
        spark.port(8080);
        spark.staticFiles.location("/public");
        spark.init();
        return spark;
    }

    @Bean
    public TemplateEngine sparkTemplateEngine() {
        return new FreeMarkerEngine();
    }

    @Bean
    public BaseSparkHandlerConfig baseSparkHandlerConfig(
            spark.Service sparkHttp,
            TemplateEngine sparkTemplateEngine,
            ObjectMapper sparkObjectMapper) {
        var config = new BaseSparkHandlerConfig();
        config.setMapper(sparkObjectMapper);
        config.setSpark(sparkHttp);
        config.setTemplateEngine(sparkTemplateEngine);
        return config;
    }

    // security

    @Bean
    public OpenIdService openIdService(Dotenv dotenv, RestTemplate restTemplate, JwtUtils jwtUtils) {
        var responseType = "code";
        var scope = "email openid profile";

        var config = new OpenIdConfig(
                responseType,
                scope,
                dotenv.get("LOGIN_OPENID_CLIENT_ID"),
                dotenv.get("LOGIN_OPENID_CLIENT_SECRET"),
                dotenv.get("LOGIN_OPENID_REDIRECT_URL"));

        var oauth2Api = new RestTemplateOAuth2Api(restTemplate);

        return new GoogleOpenIdService(config, oauth2Api, jwtUtils);
    }

    @Bean
    public SparkSecurityUtils sparkSecurityUtils(
            OpenIdService openIdService,
            PersonService personService,
            ProfileService profileService,
            PermissionService permissionService) {
        return new SparkSecurityUtilsImpl(openIdService, personService,
                profileService, permissionService);
    }

}
