package archetypes.app.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import archetypes.app.demo.DemoService;
import archetypes.app.http.spark.handlers.BaseSparkHandler;
import archetypes.app.http.spark.handlers.BaseSparkHandlerConfig;
import archetypes.app.http.spark.handlers.api.DemoSparkApi;
import archetypes.app.http.spark.handlers.api.PersonSparkApi;
import archetypes.app.http.spark.handlers.api.SecuritySparkApi;
import archetypes.app.http.spark.handlers.view.DemoSparkView;
import archetypes.app.http.spark.handlers.view.HomeSparkView;
import archetypes.app.http.spark.handlers.view.OpenIdCallbackSparkView;
import archetypes.app.http.spark.utils.security.SparkSecurityUtils;
import archetypes.app.person.PersonService;

@Configuration
public class Handlers {

    // VIEW

    @Bean
    public BaseSparkHandler demoSparkView(BaseSparkHandlerConfig config) {
        return new DemoSparkView(config);
    }

    @Bean
    public BaseSparkHandler homeSparkView(BaseSparkHandlerConfig config) {
        return new HomeSparkView(config);
    }

    @Bean
    public BaseSparkHandler openIdCallback(BaseSparkHandlerConfig config) {
        return new OpenIdCallbackSparkView(config);
    }

    // API

    @Bean
    public BaseSparkHandler demoSparkApi(BaseSparkHandlerConfig config, DemoService demoService) {
        return new DemoSparkApi(config, demoService);
    }

    @Bean
    public BaseSparkHandler personSparkApi(BaseSparkHandlerConfig config, PersonService personService) {
        return new PersonSparkApi(config, personService);
    }

    @Bean
    public BaseSparkHandler securitySparkApi(BaseSparkHandlerConfig config, SparkSecurityUtils securityUtils) {
        return new SecuritySparkApi(config, securityUtils);
    }

}
