package archetypes.app.persistence.postgres;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;

import archetypes.app.person.Person;
import archetypes.app.person.PersonFilters;
import archetypes.app.person.PersonRepository;

@SuppressWarnings({ "java:S1192", "java:S2259" })
public class PersonRepoPostgres extends BaseRepoPostgres implements PersonRepository {

    private static final String QUERY_PROJECTION = "p.id, p.profile_id, p.rut, p.email, p.first_name, p.last_name, p.birthday, p.active";

    private static final RowMapper<Person> ROW_MAPPER;
    static {
        ROW_MAPPER = new RowMapper<>() {
            @Override
            public Person mapRow(ResultSet rs, int rowNum) throws SQLException {
                var person = new Person();
                person.setId(rs.getLong("id"));
                person.setProfileId(rs.getInt("profile_id"));
                person.setRut(rs.getInt("rut"));
                person.setEmail(rs.getString("email"));
                person.setFirstName(rs.getString("first_name"));
                person.setLastName(rs.getString("last_name"));
                var birthDayDate = rs.getDate("birthday");
                person.setBirthday(birthDayDate != null ? birthDayDate.toLocalDate() : null);
                person.setActive(rs.getBoolean("active"));
                return person;
            }
        };
    }

    public PersonRepoPostgres(NamedParameterJdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }

    @Override
    public List<Person> getAll() {
        var query = "select %s from person p";
        query = String.format(query, QUERY_PROJECTION);
        return this.jdbcTemplate.query(query, ROW_MAPPER);
    }

    @Override
    public List<Person> findByName(String name) {
        var query = """
                SELECT %s
                FROM person p
                where CONCAT(p.first_name, p.last_name) ilike :searchString
                    """;
        query = String.format(query, QUERY_PROJECTION);
        var params = new MapSqlParameterSource()
                .addValue("searchString", String.format("%%%s%%", name));
        return this.jdbcTemplate.query(query, params, ROW_MAPPER);
    }

    @Override
    public List<Person> findByFilters(PersonFilters filters) {

        var queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT ")
                .append(QUERY_PROJECTION)
                .append(" FROM person p WHERE 1 = 1 ");

        var params = new MapSqlParameterSource();

        if (filters.getId() != null) {
            queryBuilder.append("AND p.id = :id ");
            params.addValue("id", filters.getId());
        }

        if (filters.getRut() != null) {
            queryBuilder.append("AND p.rut = :rut ");
            params.addValue("rut", filters.getRut());
        }

        if (filters.getEmail() != null) {
            queryBuilder.append("AND p.email = :email ");
            params.addValue("email", filters.getEmail());
        }

        if (filters.getName() != null) {
            queryBuilder.append("AND CONCAT(p.first_name, p.last_name) ilike :name ");
            params.addValue("name", filters.getName());
        }

        if (filters.getBeginBirthday() != null) {
            queryBuilder.append("AND p.birthday >= :beginBirthDay ");
            params.addValue("beginBirthDay", filters.getBeginBirthday());
        }

        if (filters.getEndBirthday() != null) {
            queryBuilder.append("AND p.birthday <= :endBirthDay ");
            params.addValue("endBirthDay", filters.getEndBirthday());
        }

        if (filters.getIsActivate() != null) {
            queryBuilder.append("AND p.active = :isActive ");
            params.addValue("isActive", filters.getIsActivate());
        }

        queryBuilder.append("ORDER BY p.first_name ASC, p.last_name ASC ");

        return this.jdbcTemplate.query(queryBuilder.toString(), params, ROW_MAPPER);
    }

    @Override
    public Optional<Person> findByEmail(String email) {
        var query = """
                SELECT %s FROM person p 
                WHERE p.email = :email
                """;
        query = String.format(query, QUERY_PROJECTION);
        var params = new MapSqlParameterSource()
        .addValue("email", email);
        try {
            return Optional.of(this.jdbcTemplate.queryForObject(query, params, ROW_MAPPER));
        }catch(EmptyResultDataAccessException ex) {
            return Optional.empty();
        }
    }

    @Override
    public Person insert(Person person) {

        var query = """
                INSERT INTO person
                (profile_id, rut, email, first_name, last_name, birthday, active)
                VALUES(:profileId, :rut, :email, :firstName, :lastName, :birthDay, :active)
                """;

        var params = new MapSqlParameterSource()
                .addValue("profileId", person.getProfileId())
                .addValue("rut", person.getRut())
                .addValue("email", person.getEmail())
                .addValue("firstName", person.getFirstName())
                .addValue("lastName", person.getLastName())
                .addValue("birthDay", person.getBirthday())
                .addValue("active", person.getActive());

        var keyHolder = new GeneratedKeyHolder();
        this.jdbcTemplate.update(query, params, keyHolder);

        person.setId((long) keyHolder.getKeys().get("id"));
        return person;
    }

    @Override
    public Person update(Person person) {

        var query = """
                UPDATE person
                SET profile_id=:profileId, rut=:rut, first_name=:firstName,
                    last_name=:lastName, birthday=:birthDay, active=:active
                WHERE id=:id
                """;

        var params = new MapSqlParameterSource()
                .addValue("profileId", person.getProfileId())
                .addValue("rut", person.getRut())
                .addValue("email", person.getEmail())
                .addValue("firstName", person.getFirstName())
                .addValue("lastName", person.getLastName())
                .addValue("birthDay", person.getBirthday())
                .addValue("active", person.getActive())
                .addValue("id", person.getId());

        this.jdbcTemplate.update(query, params);

        return person;
    }

    @Override
    public void delete(long id) {
        var query = """
                DELETE FROM person
                WHERE id=:id;
                    """;

        var params = new MapSqlParameterSource()
                .addValue("id", id);

        this.jdbcTemplate.update(query, params);
    }

}
