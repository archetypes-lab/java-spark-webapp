package archetypes.app.persistence.postgres;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import archetypes.app.profile.Profile;
import archetypes.app.profile.ProfileRepository;

@SuppressWarnings({ "java:S1192", "java:S2259" })
public class ProfileRepoPostgres extends BaseRepoPostgres implements ProfileRepository {

    private static final String QUERY_PROJECTION = "pro.id, pro.name";

    private static final RowMapper<Profile> ROW_MAPPER;
    static {
        ROW_MAPPER = new RowMapper<>() {
            @Override
            public Profile mapRow(ResultSet rs, int rowNum) throws SQLException {
                var profile = new Profile();
                profile.setId(rs.getInt("id"));
                profile.setName(rs.getString("name"));
                return profile;
            }
        };
    }

    public ProfileRepoPostgres(NamedParameterJdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }

    @Override
    public Profile findById(Integer id) {
        var query = """
                SELECT %s FROM profile pro
                WHERE pro.id = :id
                """;
        query = String.format(query, QUERY_PROJECTION);

        var params = new MapSqlParameterSource()
                .addValue("id", id);

        return this.jdbcTemplate.queryForObject(query, params, ROW_MAPPER);
    }

}
