package archetypes.app.persistence.postgres;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import archetypes.app.permission.Permission;
import archetypes.app.permission.PermissionRepository;

public class PermissionRepoPostgres extends BaseRepoPostgres implements PermissionRepository {

    private static final String QUERY_PROJECTION = "per.id, per.code, per.description";

    private static final RowMapper<Permission> ROW_MAPPER;
    static {
        ROW_MAPPER = new RowMapper<>() {

            @Override
            public Permission mapRow(ResultSet rs, int rowNum) throws SQLException {
                var permission = new Permission();
                permission.setId(rs.getInt("id"));
                permission.setCode(rs.getString("code"));
                permission.setDescription(rs.getString("description"));
                return permission;
            }

        };
    }

    public PermissionRepoPostgres(NamedParameterJdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }

    @Override
    public List<Permission> findByProfileId(Integer profileId) {
        var query = """
                select %s from permission per
                join permission_profile pp on (per.id = pp.permission_id)
                where pp.profile_id = :profileId
                    """;
        query = String.format(query, QUERY_PROJECTION);

        var params = new MapSqlParameterSource()
                .addValue("profileId", profileId);

        return this.jdbcTemplate.query(query, params, ROW_MAPPER);
    }

}
