package archetypes.app.openid;

import lombok.Data;

@Data
public class User {

    private String email;

    private String firstName;

    private String lastName;

    private String pictureUrl;

    private String locale;

    public String getName() {
        return String.format("%s %s", firstName, lastName).trim();
    }

}
