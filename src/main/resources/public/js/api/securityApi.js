((global, axios) => {
  global.securityApi = {
    getOpenIdLoginUrl: async function () {
      const response = await axios.get("/api/v1/security/openid-url");
      return response.data.url;
    },
    receiveOpenIdCallback: async function (code, state) {
      const openIdCallback = {
        code: code,
        state: state,
      };
      const response = await axios.post(
        "/api/v1/security/openid-callback",
        openIdCallback
      );
      return response.data;
    },
    getIdentity: async function () {
      const response = await axios.get("/api/v1/security/identity");
      return response.data;
    },
    logout: async function () {
      await axios.get("/api/v1/security/logout");
    },
  };
})(window, axios);
