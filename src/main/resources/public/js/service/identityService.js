((global, securityApi) => {
  global.identityService = {
    identity: null,
    isLoggedIn: async function () {
      if (this.identity == null) {
        await this.loadIdentity();
      }
      return this.identity != null;
    },
    getIdentity: async function () {
      if (this.identity == null) {
        await this.loadIdentity();
      }
      return this.identity;
    },
    loadIdentity: async function () {
      const identity = await securityApi.getIdentity();
      this.identity = identity;
    },
  };
})(window, securityApi);
