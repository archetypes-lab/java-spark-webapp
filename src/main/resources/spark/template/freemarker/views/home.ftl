<#import "../macro/template1.ftl" as template1>

<#assign headExtraContent>

  <script src="https://unpkg.com/primevue@3.15/button/button.min.js"></script>

  <#noparse>
    <script type="module" type="text/javascript">
      import vuecomp from "./js/vue_component.js";

      const { createApp, ref } = Vue;

      const App = {
        data: function() {
          return {
            loading: true,
            identity: null,
          } 
        },
        components: {
          "p-button": primevue.button,
        },
        created: async function() {
          const isLoggedIn = await identityService.isLoggedIn();
          if (isLoggedIn) {
            this.identity = await identityService.getIdentity();
          }
          this.loading = false;
        },
        methods: {
          async goToLogin() {
            const url = await securityApi.getOpenIdLoginUrl();
            console.log(`url: ${url}`);
            window.location.href = url;
          },
          async logout() {
            await securityApi.logout();
            window.location.href = '/';
          }
        }
      };

      createApp(App).use(primevue.config.default).mount("#app");
    </script>

    <style>
      [v-cloak] {
        display: none;
      }
    </style>
  </#noparse>

</#assign>

<@template1.page headExtra=headExtraContent>
  <div id="app" v-cloak>

    <template v-if="loading">
      Loading...
    </template>
    <template v-else>

      <template v-if="identity">
        <h2>Bienvenido {{identity.person.firstName}} </h2>
        <p-button v-on:click="logout" label="Cerrar Sesión" />
      </template>

      <template v-else>
        <p-button v-on:click="goToLogin" label="Iniciar Sesión" /> 
      </template>

    </template>

  </div>
</@template1.page>
