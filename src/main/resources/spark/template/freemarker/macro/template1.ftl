<#import "main.ftl" as main>

<#macro page headExtra="" >

    <#assign headExtraContent> 

        ${headExtra}

    </#assign>


    <@main.page headExtra=headExtraContent>

        <div class="header">
            <h1>Header</h1>
        </div>

        <div class="content">
            <#nested />
        </div>

        <div class="footer">
            <h3>Footer</h3>
        </div>

    </@main.page>

</#macro>
