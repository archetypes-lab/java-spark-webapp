<#macro page headExtra="">

<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8" />
  <title>PrimeVue Demo</title>

  <link rel="icon" type="image/x-icon" href="/img/favicon.ico">

  <link href="https://unpkg.com/primevue@3.15/resources/themes/lara-light-indigo/theme.css" rel="stylesheet" />
  <link href="https://unpkg.com/primevue@3.15/resources/primevue.min.css" rel="stylesheet" />
  <link href="https://unpkg.com/primeicons@5.0/primeicons.css" rel="stylesheet" />

  <script src="https://unpkg.com/vue@3.2"></script>
  <script src="https://unpkg.com/primevue@3.15/core/core.min.js"></script>
  <script src="https://unpkg.com/axios@0.27/dist/axios.min.js"></script>

  <script src="/js/api/securityApi.js"></script>
  <script src="/js/service/identityService.js"></script>

  ${headExtra}

</head>
  
<body>
    <div class="mainContentWrapper">
      <#nested />
    </div>
</body>

</html>

</#macro>
